/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ass2;

import ilog.concert.IloException;

/**
 *
 * @author nicol
 */
public class Main {
 
    public static void main(String[] args) throws IloException{
        // Populates the data of the problem
        int nGenerators = 31;
        int nPeriods = 24;
        
        double commitmentCost[] = new double[nGenerators];
        
        double loadsheddingCost[] = new double[nPeriods];
        
        double marginalCost[] = new double[nGenerators];
        
        double startupCost[] = new double[nGenerators];
        
        double maximumpowerOutput[] = new double[nGenerators];
        
        double minimumpowerOutput[] = new double[nGenerators];
        
        double rampupLimits[] = new double[nGenerators];
        
        double rampdownLimits[] = new double[nGenerators];
        
        double powerDemand[] = new double[nPeriods];
        
        
        // Creates an instance of the capacity planning problem
        UnitCommitmentProblem ucp = new UnitCommitmentProblem(nGenerators, nPeriods, commitmentCost, loadsheddingCost, marginalCost, startupCost, maximumpowerOutput, minimumpowerOutput, rampupLimits, rampdownLimits, powerDemand);
        
        // Creates the Master Problem
        MasterProblem mp = new MasterProblem(ucp);
        mp.solve();
        System.out.println("Optimal Benders objective value = "+mp.getObjective());
        mp.printSolution();
       
        // Checks if the results are consistent with solving the full problem
        //We don't have a FullModel here ---------------- 
        //FullModel m = new FullModel(ucp);
        //m.solve();
        
    }
}
