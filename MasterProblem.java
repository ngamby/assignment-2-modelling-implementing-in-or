/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ass2;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/**
 *
 * @author nicol
 */
public class MasterProblem {
    
    private final IloCplex model;
    private final IloNumVar u[][];
    private final IloNumVar c[][];
    private final IloNumVar phi;
    private final UnitCommitmentProblem ucp;
    
    
    
    public MasterProblem(UnitCommitmentProblem ucp) throws IloException {
        this.ucp = ucp;
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. Creates the decision variables
        u = new IloNumVar[ucp.getnGenerators()][ucp.getnPeriods()];
        c = new IloNumVar[ucp.getnGenerators()][ucp.getnPeriods()];
        
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnPeriods(); t++){
                u[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }            
        }
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnPeriods(); t++){
                c[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        this.phi = model.numVar(0, Double.POSITIVE_INFINITY,"phi");
        
        
        // 3. Creates the objective function
        // Creates an empty linear numerical expression (linear equation)
        IloLinearNumExpr obj = model.linearNumExpr();
        // Adds terms to the equation
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnPeriods(); t++){
                obj.addTerm(1, c[g-1][t-1]);
            }
        }
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnPeriods(); t++){
                obj.addTerm(ucp.getcommitmentCost()[g-1], u[t-1][g-1]);
            }
        }
        obj.addTerm(1, phi);
        
        // Tells cplex to minimize the objective function
        model.addMinimize(obj);     
        
        

//KIG PÅ HANS BD eksempel uden integer
//KIG PÅ HANS BD eksempel uden integer
//KIG PÅ HANS BD eksempel uden integer
    }       
}