/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ass2;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;

/**
 *
 * @author nicol
 */
public class FeasibilitySubProblem {
    
    private final IloCplex model;
    private final IloNumVar y[][];
    private final IloNumVar v1[];
    private final IloNumVar v2[];
    private final UnitCommitmentProblem ucp;
    private final IloRange capacityConstraints[];
    private final IloRange demandConstraints[];
    
    /**
    * Creates the LP model for the feasibility subproblem.
     * Particularly, this class represents the dual (the one with slack and surplus variables).
     * @param cpp
     * @param X a solution to MP
     * @throws IloException 
     */
    public FeasibilitySubProblem(UnitCommitmentProblem ucp, double X[]) throws IloException {
        this.ucp = ucp;
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
}
