/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ass2;

/**
 *
 * @author nicol
 */
public class UnitCommitmentProblem {
    private final int nGenerators;
    private final int nPeriods;
    private final double commitmentCost[];
    private final double loadsheddingCost[];
    private final double marginalCost[];
    private final double startupCost[];
    private final double maximumpowerOutput[];
    private final double minimumpowerOutput[];
    private final double rampupLimits[];
    private final double rampdownLimits[];
    private final double powerDemand[];
    
    public FacilityLocationProblem(int nGenerators, int nPeriods, double[] commitmentCost, double[] loadsheddingCost, double[] marginalCost, double[] startupCost, double[] maximumpowerOutput, double[] minimumpowerOutput, double[] rampupLimits, double[] rampdownLimits, double[] powerDemand) {
        this.nGenerators = nGenerators;
        this.nPeriods = nPeriods;
        this.commitmentCost = commitmentCost;
        this.loadsheddingCost = loadsheddingCost;
        this.marginalCost = marginalCost;
        this.startupCost = startupCost;
        this.maximumpowerOutput = maximumpowerOutput;
        this.minimumpowerOutput = minimumpowerOutput;
        this.rampupLimits = rampupLimits;
        this.rampdownLimits = rampdownLimits;
        this.powerDemand = powerDemand    
    }
    
    public int getnGenerators(){
        return nGenerators;
    }     
    public int getnPeriods(){
        return nPeriods;
    }
    public double[] getcommitmentCost(){
        return commitmentCost;
    }
    public double[] loadsheddingCost(){
        return loadsheddingCost;
    }        
    public double[] marginalCost(){
        return marginalCost;
    }
    public double[] startupCost(){
        return startupCost;
    }
    public double[] maximumpowerOutput(){
        return maximumpowerOutput;
    }
    public double[] minimumpowerOutput(){
        return minimumpowerOutput;
    }
    public double[] rampupLimits(){
        return rampupLimits;
    }
    public double[] rampdownLimits(){
        return rampdownLimits;
    }
    public double[] powerDemand(){
        return powerDemand;
    }
}
